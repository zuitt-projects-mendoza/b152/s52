let collection = [];

//Write the queue functions below.
//NOTE: DO NOT USE ANY ARRAY METHODS. YOU CAN USE .length property

// List of array methods that you SHOULD NOT use:
// concat(), copyWithin(), entries(), every(), fill(), filter(), find(), findIndex(), from(), includes(), indexOf(), isArray(), join(), keys(), lastIndexOf(), map(), pop(), push(), reduce(), reduceRight(), reverse(), shift(), slice(), some(), sort(), splice(), toString(), unshift(), valueOf()

//Exception: forEach()
//NOTE: USE return KEYWORD AND NOT console.log() FOR RETURNING VALUES

function print(){

	// Return the collection array.

	return collection
}


function enqueue(element){

	collection[collection.length] = element

	return collection

}

function dequeue(){

	for (let i = 1; i < collection.length; i++) collection[i - 1] = collection[i];

		collection.length--;

	return collection

}

function front(){

	return collection[0];

}

function size(){

	return collection.length

}

function isEmpty(){

	if (collection === undefined || collection.length == 0) {

		return true;

	} else {

		return false
	}
	
}



module.exports = {

print,
enqueue,
dequeue,
front,
size,
isEmpty

};