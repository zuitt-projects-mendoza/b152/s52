let collection = ["a","b","c","d","e"];

function AddToStack(element){

	collection.push(element)

	return collection;

}

AddToStack('Chocolate');

function removeFromStack(){

	collection.pop();

	return collection;

}

removeFromStack();

function peek(){

	return collection[collection.length-1];

}

peek();

function getLength(){

	return collection.length

}